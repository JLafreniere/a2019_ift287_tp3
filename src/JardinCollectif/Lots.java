package JardinCollectif;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.TypedQuery;

public class Lots {

	private TypedQuery<Lot> stmtGet;
	private TypedQuery<Lot> stmtGetAll;
	
	private Connexion cx;
	
	public Lots(Connexion _cx) {
		this.cx = _cx;
		this.stmtGet = cx.getConnection().createQuery(
                "select l from Lot l where nomLot = :nomLot",
                Lot.class
                );

		this.stmtGetAll = cx.getConnection().createQuery("select l from Lot l", Lot.class);
	}
	
	public List<Lot> getAllLots() {
        List<Lot> res = stmtGetAll.getResultList();
        return res;
	}
	
	
	public Lot ajouter(Lot lot){
		cx.getConnection().persist(lot);
		return lot;
	}
	
	public void supprimer(Lot x) {
	    cx.getConnection().remove(x);
	}

    public boolean existe(String nomLot) {
        stmtGet.setParameter("nomLot", nomLot);
        return !stmtGet.getResultList().isEmpty();
    }

	public Lot getLot(String nomLot) {
        stmtGet.setParameter("nomLot", nomLot);
        return stmtGet.getResultList().get(0);
	}

	public List<Lot> getMembership(Membre m) {
		List<Lot> lots = getAllLots();
		List<Lot> res = new ArrayList<Lot>();
		for(Lot l : lots) {
			for(Membre membre : l.getMembresAcceptes()) {
				if(membre.getNoMembre() == m.getNoMembre()) {
					res.add(l);
				}
			}
		}
		return res;
	}


}
