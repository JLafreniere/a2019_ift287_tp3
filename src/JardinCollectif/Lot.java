package JardinCollectif;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Lot {
    
	@Id
    @GeneratedValue
    private long _id;
      
	private String nomLot;
	private List<Membre> membresAcceptes;
	private List<Membre> demandes;
	private List<Plantation> plantations;
	private int nbMaxMembre;
	
	public Lot(String nomLot, int nbMaxMembre) {
		this.nomLot = nomLot;
		this.nbMaxMembre = nbMaxMembre;
		this.membresAcceptes = new ArrayList<Membre>();
		this.demandes = new ArrayList<Membre>();
		this.plantations = new ArrayList<Plantation>();
	}
	
	public void ajouterMembre(Membre m) {
		demandes.add(m);
	}
	
	public void accepterDemande(int noMembre) {
		
		List<Membre> toRemove = new ArrayList<Membre>();
		for(Membre m : demandes) {
			if(m.getNoMembre() == noMembre) {
				toRemove.add(m);
				membresAcceptes.add(m);
			}
		}
		demandes.removeAll(toRemove);
		
	}
	
	public void refuserDemande(int noMembre) {
		List<Membre> toRemove = new ArrayList<Membre>();
		for(Membre m : demandes) {
			if(m.getNoMembre() == noMembre) {
				toRemove.add(m);
			}
		}
		
		demandes.removeAll(toRemove);
	}
	
	public void ajouterPlantation(Plantation p) {
		plantations.add(p);
	}
	
	public void recolterPlantes() {
		List<Plantation> toRemove = new ArrayList<Plantation>();
		for(Plantation pl : plantations) {
			if(pl.getDateRecoltePrevue().before(new Date())) {
				toRemove.add(pl);
			}
		}
		plantations.removeAll(toRemove);
	}
	
	public long getID()
    {
        return _id;
    }

	public String getNomLot() {
		return nomLot;
	}

	public void setNomLot(String nomLot) {
		this.nomLot = nomLot;
	}

	public int getNbMaxMembre() {
		return nbMaxMembre;
	}
	
    public List<Membre> getMembresAcceptes() {
		return membresAcceptes;
	}
    
    public List<Membre> getDemandes() {
		return demandes;
	}


	public List<Plantation> getPlantations() {
		return plantations;
	}
	
	void setNbMaxMembre(int nbMaxMembre) {
		this.nbMaxMembre = nbMaxMembre;
	}
}
