package JardinCollectif;
import java.util.List;

import javax.persistence.TypedQuery;

public class Plantes {

	private TypedQuery<Plante> stmtGet;
	private TypedQuery<Plante> stmtGetAll;
	private Connexion cx;

	public Plantes(Connexion _cx) {
		this.cx = _cx;
		this.stmtGet = cx.getConnection().createQuery("select p from Plante p where nomPlante=:nomPlante", Plante.class);
		this.stmtGetAll = cx.getConnection().createQuery("select p from Plante p", Plante.class);
	}
	
	public List<Plante> getAllPlantes() {
		return stmtGetAll.getResultList();
	}
	
	public Plante ajouter(Plante plante) {
        cx.getConnection().persist(plante);
        return plante;
    }
	
	public void supprimer(Plante x) {
		x.setDisponible(false);
    }
	
    public boolean existe(String nomPlante) {
    	return getPlante(nomPlante) != null;
    }
	
    public Plante getPlante(String nomPlante) {
        stmtGet.setParameter("nomPlante", nomPlante);
        List<Plante> plante = stmtGet.getResultList();
        if(!plante.isEmpty()) {
        	return plante.get(0);
        }
        return null;
 
    }
}
