package JardinCollectif;

import java.util.List;

import javax.persistence.TypedQuery;

public class Membres {
	private TypedQuery<Membre> stmtGet;
	private TypedQuery<Membre> stmtGetAllMembres;
	private Connexion cx;
	
	public Membres(Connexion _cx) {
		this.cx = _cx;
		this.stmtGet = cx.getConnection().createQuery("select m from Membre m where noMembre=:noMembre", Membre.class);
		this.stmtGetAllMembres = cx.getConnection().createQuery("select m from Membre m", Membre.class);
	}

	public Membre ajouter(Membre membre){
        cx.getConnection().persist(membre);
        return membre;
    }
	
	public void supprimer(Membre x) {
        cx.getConnection().remove(x);
    }
	
    public Membre getMembre(int noMembre) {
        stmtGet.setParameter("noMembre", noMembre);
        List<Membre> res = stmtGet.getResultList();
        if(! res.isEmpty()) {
        	return res.get(0);
        }
        return null;
    }
    
    public List<Membre> getAllMembres() {
        List<Membre> res = stmtGetAllMembres.getResultList();
        return res;
    }
    
    public boolean existe(int noMembre) {
        return getMembre(noMembre) != null;
    }
}
