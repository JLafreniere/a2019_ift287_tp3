package JardinCollectif;

import java.sql.SQLException;
import java.util.List;

public class GestionPlante {
	private Plantes plantes;
	private Lots lots;
	private Connexion cx;
	
	public GestionPlante(Connexion cx) throws SQLException {
		this.cx = cx;
		plantes = new Plantes(cx);
		lots = new Lots(cx);
	}
	
	public List<Plante> getAllPlantes() {
		return plantes.getAllPlantes();
	}
	
	public void ajouterPlante(String nomPlante, int tempsDeCulture) throws SQLException, IFT287Exception {
		try {
			cx.demarreTransaction();
			if(!plantes.existe(nomPlante)){
				plantes.ajouter(new Plante(nomPlante, true, tempsDeCulture));
			} else {
				throw new IFT287Exception("Une plante de ce nom existe deja");
			}
			cx.commit();
		} catch (Exception e) {
			cx.rollback();
			throw e;
		}
		
	}
	
	public void retirerPlante(String nomPlante) throws SQLException, IFT287Exception {
		try {
			cx.demarreTransaction();
			boolean inUse = false;
			List<Lot> listeLots = lots.getAllLots();
			for(Lot l : listeLots) {
				List<Plantation> pl = l.getPlantations();
				for(Plantation p : pl) {
					if(p.getPlante().getNomPlante() == nomPlante) {
						inUse = true;
					}
				}
			}
			
			if(inUse) { 
				throw new IFT287Exception("La plante est en culture et ne peut pas etre supprimee");
			}
			
			if(plantes.existe(nomPlante)){
				plantes.supprimer(plantes.getPlante(nomPlante));
			} else {
				throw new IFT287Exception("Aucune plante de ce nom");
			}
			cx.commit();
		} catch (Exception e) {
			cx.rollback();
			throw e;
		}
	
	}
}
