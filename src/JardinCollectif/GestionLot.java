package JardinCollectif;

import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

public class GestionLot {
	private Lots lots;
	private Membres membres;
	private Plantes plantes;
	private Connexion cx;
	
	public GestionLot(Connexion cx) throws SQLException {
		this.cx = cx;
		lots = new Lots(cx);
		membres = new Membres(cx);
		plantes = new Plantes(cx);
	}
	

	
	public void ajouterLot(String nomLot, int nbMaxMembres) throws SQLException, IFT287Exception {
		try {
			cx.demarreTransaction();
			if(!lots.existe(nomLot)){	
			    Lot l = new Lot(nomLot, nbMaxMembres);
				lots.ajouter(l);
			} else {
				throw new IFT287Exception("Un lot du meme nom existe deja");
			}
			cx.commit();
		} catch (Exception e) {
			cx.rollback();
			throw e;
		}
		
		
	}
	
	public void supprimerLot(String nomLot) throws IFT287Exception, Exception {
		Lot ll = lots.getLot(nomLot);
		try{
			cx.demarreTransaction();
			if(lots.existe(ll.getNomLot())) {
				lots.supprimer(ll);
			} else {
				throw new IFT287Exception("Le lot a supprimer n'existe pas");
			}
			cx.commit();
		} catch (Exception e) {
			cx.rollback();
			throw e;
		}
	}
	
	public void rejoindreLot(String nomLot, int noMembre) throws SQLException, IFT287Exception {
		try {
			cx.demarreTransaction();
			if(!lots.existe(nomLot)) {
				throw new IFT287Exception("Aucun lot ne porte ce nom");
			}
			if(!membres.existe(noMembre)) {
				throw new IFT287Exception("Aucun membre n'a ce numero");
			}
			
			Lot l = lots.getLot(nomLot);
			Membre m = membres.getMembre(noMembre);
			l.getDemandes().add(m);
			
			cx.commit();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			cx.rollback();
			throw e;
		}
	}
	
	public Lot getLot(String nomLot) {
		return lots.getLot(nomLot);
	}
	
	public void planterPlante(String nomPlante, String nomLot, int noMembre, int nbExemplaires, Date datePlantation) throws IFT287Exception, SQLException {
		try {
			cx.demarreTransaction();
			if(!lots.existe(nomLot)) {
				throw new IFT287Exception("Aucun lot ne porte ce nom");
			}
			if(!membres.existe(noMembre)) {
				throw new IFT287Exception("Aucun membre n'a ce numero");
			}
			if(!plantes.existe(nomPlante)) {
				throw new IFT287Exception("Aucune plante ne porte ce nom");
			}		
			if(nbExemplaires < 0) {
				throw new IFT287Exception("Le nombre d'exemplaires doit etre positif");
			}
			
			Lot l = lots.getLot(nomLot);
			boolean isAMember = false;
			for(Membre m : l.getMembresAcceptes()) {
				if(m.getNoMembre() == noMembre) {
					isAMember = true;
				}
			}
			
			if(!isAMember) {
				throw new IFT287Exception("Le membre ne fait pas partie de ce lot");
			}
			
			Plante p = plantes.getPlante(nomPlante);
			
			if(!p.isDisponible()) throw new IFT287Exception("La plante n'est pas disponible");
			
			Plantation pl = new Plantation(plantes.getPlante(nomPlante), nbExemplaires, datePlantation);
			cx.getConnection().persist(pl);
			Lot lot = lots.getLot(nomLot);
			lot.ajouterPlantation(pl);
			cx.commit();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			cx.rollback();
			throw e;
		}
	}
	
	
		
		public void accepterDemande(String nomLot, int noMembre) throws SQLException, IFT287Exception {
		try {
			cx.demarreTransaction();
			if(!lots.existe(nomLot)) {
				throw new IFT287Exception("Aucun lot ne porte ce nom");
			}
			if(!membres.existe(noMembre)) {
				throw new IFT287Exception("Aucun membre n'a ce numero");
			}
			
			Lot l = lots.getLot(nomLot);
			boolean hasDemande = false;
			for(Membre m : l.getDemandes()) {
				if(m.getNoMembre() == noMembre) {
					hasDemande = true;
				}
			}
			
			if(!hasDemande) {
				throw new IFT287Exception("Aucune demande de ce membre pour ce lot");
			}
			
			l.accepterDemande(noMembre);
			
			cx.commit();
		} catch (Exception e) {
			cx.rollback();
			throw e;
		}
	}
	
	
	//TODO: Check whether a demand EXISTS
	public void refuserDemande(String nomLot, int noMembre) throws SQLException, IFT287Exception {
		try { 	
			cx.demarreTransaction();
			if(!lots.existe(nomLot)) {
				throw new IFT287Exception("Aucun lot ne porte ce nom");
			}
			if(!membres.existe(noMembre)) {
				throw new IFT287Exception("Aucun membre n'a ce numero");
			}
			
			Lot l = lots.getLot(nomLot);
			boolean hasDemande = false;
			for(Membre m : l.getDemandes()) {
				if(m.getNoMembre() == noMembre) {
					hasDemande = true;
				}
			}
			
			if(!hasDemande) {
				throw new IFT287Exception("Aucune demande de ce membre pour ce lot");
			}
		
			l.refuserDemande(noMembre);
			
			cx.commit();
		} catch (Exception e) {
			cx.rollback();
			throw e;
		}
	}
	
	public void recolter(String nomPlante, String nomLot, int noMembre) throws Exception {
		try {
			cx.demarreTransaction();
			if(!lots.existe(nomLot)) {
				throw new IFT287Exception("Aucun lot ne porte ce nom");
			}
			if(!membres.existe(noMembre)) {
				throw new IFT287Exception("Aucun membre n'a ce numero");
			}
			
			Lot l = lots.getLot(nomLot);
			boolean isAMember = false;
			for(Membre m : l.getMembresAcceptes()) {
				if(m.getNoMembre() == noMembre) {
					isAMember = true;
				}
			}
			
			if(!isAMember) {
				throw new IFT287Exception("Ce membre ne fait pas partie du lot");
			}
			
			l.recolterPlantes();
			
			cx.commit();
		} catch (Exception e) {
			cx.rollback();
			throw e;
		}
	}
	
	public List<Plantation> getPlantations(String nomLot){
		Lot l = lots.getLot(nomLot);
		return l.getPlantations(); //TODO: Afficher date plantation
	}
	
	
	
	public List<Lot> getAllLots() {
		return lots.getAllLots();
	}
	
	public List<Membre> getMembersOnLot(String nomLot) {
		Lot l = lots.getLot(nomLot);
		return l.getMembresAcceptes();
	}
}
