package JardinCollectif;
import javax.persistence.*;

@Entity
public class Membre {
    
    @Id
    @GeneratedValue
    private long _id;
    
	private int noMembre;
	private boolean Admin;
	private String MotDePasse;
	private String Prenom;
	private String Nom;
	
	public long getID()
    {
        return _id;
    }
	
	public int getNoMembre() {
		return noMembre;
	}

	public void setNoMembre(int noMembre) {
		this.noMembre = noMembre;
	}

	public boolean isAdmin() {
		return Admin;
	}

	public void setAdmin(boolean admin) {
		Admin = admin;
	}

	public String getMotDePasse() {
		return MotDePasse;
	}

	public void setMotDePasse(String motDePasse) {
		MotDePasse = motDePasse;
	}

	public String getPrenom() {
		return Prenom;
	}

	public void setPrenom(String prenom) {
		Prenom = prenom;
	}

	public String getNom() {
		return Nom;
	}

	public void setNom(String nom) {
		Nom = nom;
	}

	public Membre(int _noMembre, boolean admin, String motDePasse, String prenom, String nom) {
		super();
		noMembre = _noMembre;
		Admin = admin;
		MotDePasse = motDePasse;
		Prenom = prenom;
		Nom = nom;
	}
}
