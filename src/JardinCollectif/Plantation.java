package JardinCollectif;

import javax.persistence.*;

import java.sql.Date;
import java.util.Calendar;

@Entity
public class Plantation {
    
    @Id
    @GeneratedValue
    private long _id;

    @ManyToOne
	private Plante plante;
    
	private int quantite;
	private Date datePlantation;
    
	public Plantation(Plante plante, int quantite, Date datePlantation)
    {
        super();
        this.plante = plante;
        this.quantite = quantite;
        this.datePlantation = datePlantation;
    }

    public long get_id()
    {
        return _id;
    }

    public Plante getPlante()
    {
        return plante;
    }

    public int getQuantite() {
		return quantite;
	}

	public Date getDatePlantation() {
		return datePlantation;
	}
	
	public Date getDateRecoltePrevue() {
		return addDays(getDatePlantation(), plante.getTempsDeCulture());
	}
	
	public static Date addDays(Date date, int days) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, days);
        return new Date(c.getTimeInMillis());
    }
	
}
