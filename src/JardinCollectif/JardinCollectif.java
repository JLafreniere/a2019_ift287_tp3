// Travail fait par :
//   NomEquipier1 - Matricule
//   NomEquipier2 - Matricule

package JardinCollectif;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Date;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Fichier de base pour le TP2 du cours IFT287
 *
 * <pre>
 * 
 * Vincent Ducharme
 * Universite de Sherbrooke
 * Version 1.0 - 7 juillet 2016
 * IFT287 - Exploitation de BD relationnelles et OO
 * 
 * Ce programme permet d'appeler des transactions d'un systeme
 * de gestion utilisant une base de donnees.
 *
 * Parametres du programme
 * 0- site du serveur SQL ("local" ou "dinf")
 * 1- nom de la BD
 * 2- user id pour etablir une connexion avec le serveur SQL
 * 3- mot de passe pour le user id
 * 4- fichier de transaction [optionnel]
 *           si non specifie, les transactions sont lues au
 *           clavier (System.in)
 *
 * Pre-condition
 *   - La base de donnees doit exister
 *
 * Post-condition
 *   - Le programme effectue les mises a jour associees a chaque
 *     transaction
 * </pre>
 */
public class JardinCollectif
{
    private static Connexion cx;
    private static GestionMembre gm;
    private static GestionLot gl;
    private static GestionPlante gp;

    /**
     * @param args
     */
    public static void main(String[] args) throws Exception
    {
        if (args.length < 4)
        {
            System.out.println("Usage: java JardinCollectif.JardinCollectif <serveur> <bd> <user> <password> [<fichier-transactions>]");
            return;
        }
        
        cx = null;
        
        try
        {
            // Il est possible que vous ayez a� deplacer la connexion ailleurs.
            // N'hesitez pas a� le faire!
            cx = new Connexion(args[0], args[1], args[2], args[3]);
            
            gm = new GestionMembre(cx);
            gp = new GestionPlante(cx);
            gl = new GestionLot(cx);
            
            BufferedReader reader = ouvrirFichier(args);
            String transaction = lireTransaction(reader);
            while (!finTransaction(transaction))
            {
                executerTransaction(transaction);
                transaction = lireTransaction(reader);
            }
        }
        finally
        {
            if (cx != null)
                cx.fermer();
        }
    }

    /**
     * Decodage et traitement d'une transaction
     */
    static void executerTransaction(String transaction) throws Exception, IFT287Exception
    {
        try
        {
            System.out.print(transaction);
            // Decoupage de la transaction en mots
            StringTokenizer tokenizer = new StringTokenizer(transaction, " ");
            if (tokenizer.hasMoreTokens())
            {
                String command = tokenizer.nextToken();

                if (command.equals("inscrireMembre"))
                {
                    String prenom = readString(tokenizer);
                    String nom = readString(tokenizer);
                    String password = readString(tokenizer);
                    int noMembre = readInt(tokenizer);
                    gm.inscrireMembre(prenom, nom, password, noMembre);
                }else if (command.equals("supprimerMembre"))
                {
                    int noMembre = readInt(tokenizer);
                    //TODO ne peut pas supprimer si dernier membre sur un lot, do that verification
                    gm.supprimerMembre(noMembre);
                }
                else if (command.equals("promouvoirAdministrateur"))
                {
                    int noMembre = readInt(tokenizer);
                    gm.promouvoirAdministrateur(noMembre);
                }
                else if (command.equals("ajouterLot"))
                {
                    String nomLot = readString(tokenizer);
                    int nbMaxMembres = readInt(tokenizer);
                    gl.ajouterLot(nomLot, nbMaxMembres);
                }
                else if (command.equals("supprimerLot"))
                {
                    String nomLot = readString(tokenizer);
                    gl.supprimerLot(nomLot);
                }else if (command.equals("rejoindreLot"))
                {
                    String nomLot = readString(tokenizer);
                    int noMembre = readInt(tokenizer);
                    gl.rejoindreLot(nomLot, noMembre);
                }else if (command.equals("planterPlante"))
                {
                    String nomPlante = readString(tokenizer);
                    String nomLot = readString(tokenizer);
                    int noMembre = readInt(tokenizer);
                    int nbExemplaires = readInt(tokenizer);
                    Date datePlantation = readDate(tokenizer);
                    gl.planterPlante(nomPlante, nomLot, noMembre, nbExemplaires, datePlantation);
                }else if (command.equals("recolterPlante"))
                {
                    String nomPlante = readString(tokenizer);
                    String nomLot = readString(tokenizer);
                    int noMembre = readInt(tokenizer);
                    gl.recolter(nomPlante, nomLot, noMembre);
                }
                else if (command.equals("afficherLots"))
                {
                    List<Lot> lots = gl.getAllLots();
                    System.out.println();
                    for(Lot lot : lots) {
            
                    	System.out.println("LOT: "+lot.getNomLot());
                    	List<Membre> membership = lot.getMembresAcceptes();
                    	for(Membre m : membership) {
                    		System.out.println(m.getNoMembre() + ": " + m.getPrenom() + " " + m.getNom());
                    	}
                    }
                }
                else if (command.equals("afficherPlantesLot"))
                {
                	System.out.println();
                	String nomLot = readString(tokenizer);
                	Lot l = gl.getLot(nomLot);
                	
                	try {
                    	for(Plantation pl: l.getPlantations()) {
                    		System.out.println(l.getNomLot() +" "+pl.getPlante().getNomPlante()+" "+pl.getQuantite()+" "+pl.getDatePlantation().toString()+" "+ pl.getDateRecoltePrevue().toString());
                    	}
                	} catch (Exception e) {
                		System.out.println(e.getMessage());
                		throw e;
                	}

                 
                }
                else if(command.equals("afficherMembres")) {
                	System.out.println();
                	List<Membre> membres = gm.getAllMembres();
                	for(Membre m: membres) {
                		System.out.println(m.getNoMembre()+" "+ m.getPrenom()+ " " + m.getNom() + " " + m.getMotDePasse() + " " + m.isAdmin());
                	}
                	
                }
                else if(command.equals("afficherPlantes")) {
                	System.out.println();
                	List<Plante> plantes = gp.getAllPlantes();
         
                	for(Plante p: plantes) {
                		System.out.println(p.getNomPlante()+" " + p.getTempsDeCulture() + " " + p.isDisponible());
                	}
                }
                else if (command.equals("ajouterPlante"))//
                {
                	String nomPlante = readString(tokenizer);
                	int tempsDeCulture = readInt(tokenizer);
                	gp.ajouterPlante(nomPlante, tempsDeCulture);
                }else if (command.equals("retirerPlante"))
                {
                	String nomPlante = readString(tokenizer);
                	gp.retirerPlante(nomPlante);
                }
                else if(command.equals("accepterDemande")) {
                 	String nomLot = readString(tokenizer);
                	int noMembre = readInt(tokenizer);
                	gl.accepterDemande(nomLot, noMembre);
                }
                else if(command.equals("refuserDemande")) {
                 	String nomLot = readString(tokenizer);
                	int noMembre = readInt(tokenizer);
                	gl.refuserDemande(nomLot, noMembre);
                }
                else
                {
                    System.out.println(" : Transaction non reconnue");
                }
            }
        }
        catch (Exception e)
        {
            System.out.println(" " + e.toString());
            // Ce rollback est ici seulement pour vous aider et éviter des problèmes lors de la correction
            // automatique. En theorie, il ne sert à rien et ne devrait pas apparaitre ici dans un programme
            // fini et fonctionnel sans bogues.
            cx.rollback();
        }
    }

    
    // ****************************************************************
    // *   Les methodes suivantes n'ont pas besoin d'etre modifiees   *
    // ****************************************************************

    /**
     * Ouvre le fichier de transaction, ou lit à partir de System.in
     */
    public static BufferedReader ouvrirFichier(String[] args) throws FileNotFoundException
    {
        if (args.length < 5)
            // Lecture au clavier
            return new BufferedReader(new InputStreamReader(System.in));
        else
            // Lecture dans le fichier passe en parametre
            return new BufferedReader(new InputStreamReader(new FileInputStream(args[4])));
    }

    /**
     * Lecture d'une transaction
     */
    static String lireTransaction(BufferedReader reader) throws IOException
    {
        return reader.readLine();
    }

    /**
     * Verifie si la fin du traitement des transactions est atteinte.
     */
    static boolean finTransaction(String transaction)
    {
        // fin de fichier atteinte
        return (transaction == null || transaction.equals("quitter"));
    }

    /** Lecture d'une chaine de caracteres de la transaction entree a l'ecran */
    static String readString(StringTokenizer tokenizer) throws Exception
    {
        if (tokenizer.hasMoreElements())
            return tokenizer.nextToken();
        else
            throw new Exception("Autre parametre attendu");
    }

    /**
     * Lecture d'un int java de la transaction entree a l'ecran
     */
    static int readInt(StringTokenizer tokenizer) throws Exception
    {
        if (tokenizer.hasMoreElements())
        {
            String token = tokenizer.nextToken();
            try
            {
                return Integer.valueOf(token).intValue();
            }
            catch (NumberFormatException e)
            {
                throw new Exception("Nombre attendu a la place de \"" + token + "\"");
            }
        }
        else
            throw new Exception("Autre parametre attendu");
    }

    static Date readDate(StringTokenizer tokenizer) throws Exception
    {
        if (tokenizer.hasMoreElements())
        {
            String token = tokenizer.nextToken();
            try
            {
                return Date.valueOf(token);
            }
            catch (IllegalArgumentException e)
            {
                throw new Exception("Date dans un format invalide - \"" + token + "\"");
            }
        }
        else
            throw new Exception("Autre parametre attendu");
    }

}
