package JardinCollectif;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class GestionMembre {

	private Membres membres;
	private Lots lots;
	private Connexion cx;
	
	
	public GestionMembre(Connexion cx){
		this.cx = cx;
		lots = new Lots(cx);
		membres = new Membres(cx);
	}
	
	public List<Membre> getAllMembres() {
		return membres.getAllMembres();
	}
	
	public void inscrireMembre(String prenom, String nom, String motDePasse, int noMembre) throws Exception{
		try {
			cx.demarreTransaction();
			if(membres.existe(noMembre)){
				throw new IFT287Exception("Le membre existe deja");
			} else {
				membres.ajouter(new Membre(noMembre, false, prenom, nom, motDePasse));
				cx.commit();
			}
		} catch (Exception e) {
			cx.rollback();
			throw e;
		}
	}
	
	public Membre getMembre(int noMembre) throws IFT287Exception{
		if(membres.existe(noMembre)) {
			return membres.getMembre(noMembre);
		}
		throw new IFT287Exception("Le membre n'existe pas");
	}
	
	
	public void supprimerMembre(int noMembre) {
		try {
			cx.demarreTransaction();
			Membre m = membres.getMembre(noMembre);
			List<Lot> lotsForMembre = lots.getMembership(m);
			
			for(Lot l : lotsForMembre) {
				if(l.getMembresAcceptes().size() == 1) throw new IFT287Exception("Le dernier membre d'un lot ne peut pas etre supprime");
			}
			
			if(membres.existe(noMembre)){
				membres.supprimer(membres.getMembre(noMembre));
			} else {
				throw new IFT287Exception("Le membre n'existe pas");
			}
			cx.commit();
		} catch (Exception e) {
			cx.rollback();
		}
		
	}
	
	public void promouvoirAdministrateur(int noMembre){
		try {	
			cx.demarreTransaction();
			if(membres.existe(noMembre)){
				membres.getMembre(noMembre).setAdmin(true);
				cx.commit();
			} else {
				throw new IFT287Exception("Le membre n'existe pas");
			}
		} catch (Exception e) {
			cx.rollback();
		}
	}
}
