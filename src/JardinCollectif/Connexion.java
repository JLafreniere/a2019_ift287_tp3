package JardinCollectif;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Gestionnaire d'une connexion avec une BD Objet via ObjectDB.
 * 
 * <pre>
 * 
 * Vincent Ducharme
 * Universite de Sherbrooke
 * Version 1.0 - 18 juin 2016
 * IFT287 - Exploitation de BD relationnelles et OO
 * 
 * Ce programme permet d'ouvrir une connexion avec une BD via ObjectDB.
 * 
 * Pré-condition
 *   La base de donnée ObjectDB doit etre accessible.
 * 
 * Post-condition
 *   La connexion est ouverte.
 * </pre>
 */
public class Connexion
{
    private EntityManager em;
    private EntityManagerFactory emf;

    /**
     * Ouverture d'une connexion
     * 
     * @param serveur : Le type de serveur SQL a� utiliser (Valeur : local, dinf).
     * @param bd : nom de la base de donnees
     * @param user : userid sur le serveur SQL
     * @param pass : mot de passe sur le serveur SQL
     */
    public Connexion(String serveur, String bd, String user, String pass) throws IFT287Exception
    {
    	//dinf ift287_10db ift287_10 ayn3oezs
        if (serveur.equals("local"))
        {
            emf = Persistence.createEntityManagerFactory("test.odb");
        }
        else if (serveur.equals("dinf"))
        {
            Map<String, String> properties = new HashMap<String, String>();
              properties.put("javax.persistence.jdbc.user", user);
              properties.put("javax.persistence.jdbc.password", pass);
            emf = Persistence.createEntityManagerFactory("objectdb://hibou.dinf.fsci.usherbrooke.ca:6136/"+user+"/" + bd, properties);
        }
        else
        {
            throw new IFT287Exception("Serveur inconnu");
        }
        
        em = emf.createEntityManager();
        
        System.out.println("Ouverture de la connexion :\n"
                + "Connecte sur la BD ObjectDB "
                + bd + " avec l'utilisateur " + user);
    }

    /**
     * fermeture d'une connexion
     */
    public void fermer()
    {
        em.close();
        emf.close();
        System.out.println("Connexion ferm�e");
    }
    
    public void demarreTransaction()
    {
        em.getTransaction().begin();
    }

    /**
     * commit
     */
    public void commit()
    {
        em.getTransaction().commit();
    }

    /**
     * rollback
     */
    public void rollback()
    {
        em.getTransaction().rollback();
    }

    /**
     * retourne la Connection ObjectDB
     */
    public EntityManager getConnection()
    {
        return em;
    }
}// Classe Connexion
