package JardinCollectif;

import javax.persistence.*;

@Entity
public class Plante {
    
    @Id
    @GeneratedValue
    private long _id;
    
	private String nomPlante;
	private boolean disponible;
	private int tempsDeCulture;

    public long getID()
    {
        return _id;
    }
    
	public Plante(String nomPlante, boolean disponible, int tempsDeCulture) {
		super();
		this.nomPlante = nomPlante;
		this.disponible = disponible;
		this.tempsDeCulture = tempsDeCulture;
	}
	
	public String getNomPlante() {
		return nomPlante;
	}
	 
	public void setNomPlante(String nomPlante) {
		this.nomPlante = nomPlante;
	}
	
	public boolean isDisponible() {
		return disponible;
	}
	
	public void setDisponible(boolean disponible) {
		this.disponible = disponible;
	}
	
	public int getTempsDeCulture() {
		return tempsDeCulture;
	}
	
	public void setTempsDeCulture(int tempsDeCulture) {
		this.tempsDeCulture = tempsDeCulture;
	}
}
